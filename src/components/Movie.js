const Movie = (props) => {
    return (
    <div className="container">
    <div className="card" style={{width: "18rem"}}>
    {
        props.image == null ?  <img className="card-img-top" src="https://image.tmdb.org/t/p/w500/wwemzKWzjKYJFfCeiB57q3r4Bcm.png" alt="card_image"/>: <img src={`http://image.tmdb.org/t/p/w500${props.image}`} alt="card_image" />
    }
    <div class="card-body">
      <h5 class="card-title">{props.title}</h5>
      <p class="card-text">{props.overview}</p>
      <a href="ifn" class="btn btn-primary">View details</a>
    </div>
  </div>
  </div>
    )
}

export default Movie

