const Search = (props) => {
    return (
       <div className="container">
           <div className="row">
               <section className="col s4 offset-s4">
                   <p></p>
               <form className="d-flex" action="" onSubmit={props.handleSubmit}>
                    <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" onChange={props.handleChange}/>
                 <button className="btn btn-outline-success" type="submit">Search</button>
                </form>
                <p></p>
               </section>
           </div>
       </div>
    )
}

export default Search