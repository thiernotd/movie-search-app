import Movie from "./Movie"
const MovieList = (props) => {
  return (
    <div className="container">
    <div className="container">
        <div className="row">
            {
                props.movies.map((movie, index) => {
                    return (
                        <div class="col-sm">
                        <Movie key={index} image={movie.poster_path} title={movie.title} overview={movie.overview} />
                      </div>
                       
                    )}
                )
                
            }
        </div>
    </div>
</div>
)

}

export default MovieList