import { Component } from "react"
import Nav from "./Nav"
import MovieList from "./MoviesList"
import Search from "./Search"


class App extends Component {
  
  	// Constructor
	constructor(props) {
		super(props)

		this.state = {
			movies: [],
			searchTerm: 'movie'
		}
    this.apiKey = process.env.REACT_APP_API
	}

  handleSubmit = (e) => {
    e.preventDefault()
    fetch(`https://api.themoviedb.org/3/search/movie?api_key=${this.apiKey}&query=${this.state.searchTerm}`)
    .then(data => data.json())
    .then(data => {
      console.log(data)
      this.setState({ movies: [...data.results]})
    })
  }

  handleChange = (e) => {
    this.setState({ searchTerm: e.target.value})

  }


   render() {
    return (
      <div className="App">
         <Nav />
         <Search handleSubmit={this.handleSubmit} handleChange={this.handleChange}/>
         <MovieList  movies={this.state.movies}/>
      </div>
    )
  }
}


export default App


