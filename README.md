[![pipeline status](https://gitlab.com/thiernotd/movie-search-app/badges/master/pipeline.svg)](https://gitlab.com/thiernotd/movie-search-app/-/commits/master)
# The Movie Search App

The project information [The Movie Search App](https://www.notion.so/Study-Case-Front-End-Developer-c461c0fb83614d1bb62b069669339ff6).

You can see the project result [The Movie Search App](https://movie-search-reactapp.web.app/).

## Available Scripts

In the project directory, you can run:

### `npm ci`

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


